"""This script defines the general parsing tools for the Umat Generator


After reading user configuration (file.yaml), and verified dev-files
- Write a file.f file including the desired options
- Write a file.inp file with appropriate user material, state vars, etc.
"""

import logging
import sys
import os
import copy

logging.debug('client.parser import')

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__),'../')))

##pylint: disable=wrong-import-position
import client
from client import paths
import client.utils_inp      as uin
import client.utils_devfile  as ude
import client.utils_userfile as uus

INP_NAME = client.DEFAULT_NAME+'.inp'
UMA_NAME = client.DEFAULT_NAME+'.f'

##########################################################################################

FOR_EXT = '.f'

##########################################################################################
# STRING DEFINITIONS
##########################################################################################

# This is the Place Holder String with format {} space.
# https://patorjk.com/software/taag/#p=display&h=3&f=Stop&t=place%20holder
PHS=r'''
C=======================================================================
C            _                     _           _     _             
C           | |                   | |         | |   | |            
C      ____ | | ____  ____ ____   | | _   ___ | | _ | | ____  ____ 
C     |  _ \| |/ _  |/ ___/ _  )  | || \ / _ \| |/ || |/ _  )/ ___)(_)
C     | | | | ( ( | ( (__( (/ /   | | | | |_| | ( (_| ( (/ /| |    
C     | ||_/|_|\_||_|\____\____)  |_| |_|\___/|_|\____|\____|_|    (_)
C     |_|                                                          
C     ------------------------------------------------------------------
C     {}
C=======================================================================
'''.strip()

# This is the dictionnary of contents for the Umat file
PROJECT_LINES = dict()

PROJECT_LINES["doxygen file info"]=r"""
C_______________________________________________________________________
C              ____  _   _  ____            
C             |  _ \| | | |/ _  |           
C   _______   | | | | |_| ( ( | |   _______ 
C  (_______)  | ||_/ \____|\_|| |  (_______) doxygen file info
C             |_|         (_____|
C-----------------------------------------------------------------------
C> @file PUG.f The main UMAT file
{}
C_______________________________________________________________________
""".strip()

PROJECT_LINES["props splitting"]=r"""
C_______________________________________________________________________
C              ____  _   _  ____            
C             |  _ \| | | |/ _  |           
C   _______   | | | | |_| ( ( | |   _______ 
C  (_______)  | ||_/ \____|\_|| |  (_______) properties splitting
C             |_|         (_____|
C-----------------------------------------------------------------------
      integer np_EL, np_HT, np_ES, np_RS
      real*8  par_EL({0}), par_RS({1}), par_ES({2}), par_HT({3})
      np_EL = {0}  !
      np_RS = {1}  !
      np_ES = {2}  !
      np_HT = {3}  !
      call Props_Assign(PROPS, NPROPS, par_EL, np_EL, 0                )
      call Props_Assign(PROPS, NPROPS, par_RS, np_RS, np_EL            )
      call Props_Assign(PROPS, NPROPS, par_ES, np_ES, np_EL+np_RS      )
      call Props_Assign(PROPS, NPROPS, par_HT, np_HT, np_EL+np_RS+np_ES)
C_______________________________________________________________________
""".strip()

PROJECT_LINES["files inclusions"]=r'''
C_______________________________________________________________________
C              ____  _   _  ____            
C             |  _ \| | | |/ _  |           
C   _______   | | | | |_| ( ( | |   _______ 
C  (_______)  | ||_/ \____|\_|| |  (_______) file inclusions
C             |_|         (_____|
C-----------------------------------------------------------------------
      include "{0}common/linear_algebra.f"
      include "{0}common/voigt_to_natural.f"
      include "{0}common/utils.f"
      include "{0}elasticity/{1}"
      include "{0}hardening_type/{2}"
      include "{0}equivalent_stress/{3}"
      include "{0}reference_stress/{4}"
C_______________________________________________________________________
C
'''.strip()

# This should be readapted to every possibility
PROJECT_LINES["format 200"]=r'''
C_______________________________________________________________________
C              ____  _   _  ____            
C             |  _ \| | | |/ _  |           
C   _______   | | | | |_| ( ( | |   _______ 
C  (_______)  | ||_/ \____|\_|| |  (_______) log format 200
C             |_|         (_____|
C-----------------------------------------------------------------------
200   format(/
     +        5x, '***********************************'/,
C    +        5x, '*   _    _ __  __       _______   *'/,
C    +        5x, '*  | |  | |  \/  |   /\|__   __|  *'/,
C    +        5x, '*  | |  | | \  / |  /  \  | |     *'/,
C    +        5x, '*  | |  | | |\/| | / /\ \ | |     *'/,
C    +        5x, '*  | |__| | |  | |/ ____ \| |     *'/,
C    +        5x, '*   \____/|_|  |_/_/    \_\_|     *'/,
C    +        5x, '*                                 *'/,
C    +        5x, '***********************************'/,
C    +        5x, '     in the natural notation       '/,
C    +        5x, '                                   '/,
C    +        5x, '              WITH                 '/,
C    +        5x, '      MATERIAL PARAMETERS:         '/,
C    +        5x, '                                   '/,
     +        5x, ' prop:    = ',               1pe12.5/,
     +        5x, '***********************************'
     +  /)
C_______________________________________________________________________
C
'''.strip()


##########################################################################################
# STRING REPLACEMENT
##########################################################################################

def replace_from_base(txt, ph_rep):
    """Returns the text with replaced place-holders.

    Arguments
    ---------
    txt : str
        The initial text with placeholders (see PHS)

    ph_rep : dict
        Place holder replacements.  dict items are of the form
        ph_rep[placeHoldername] = newContents

    Returns
    -------
    txt : str
        The provided instance with every `placeholder` replaced

    Raises
    ------
    Exception
        If one of the requested placeholder is not found
    """
    for key, content in ph_rep.items():
        phs = PHS.format(key)
        if phs not in txt:
            raise Exception("Place holder string not found: "+key)
        txt=txt.replace(phs, content)
    return txt


##########################################################################################
# CONFIGURATION GATHERING
##########################################################################################

def cfgfile_to_dict(cfg_path:str):
    """Returns a dictionary of project parameters given a configuration file.

    Arguments
    ---------
    cfg_path : str
        path to a configuration file

    Returns
    -------
    models: dict
        the base is the yaml-dict info from the config file which provides names
        we complete it with defiles info like `parameters_number ` or `description`
    """
    # Make sure CFG exists
    if not paths.exists(cfg_path):
        logging.error('The provided config file does not exist')
        raise Exception('The provided config file does not exist')

    # Read it
    models = uus.read(cfg_path)
    #uus.check_dict(models) # no need because the reader checks
    for model in ('elasticity',
                  'hardening_type',
                  'equivalent_stress',
                  'reference_stress',):
        model_path = os.path.join(paths.DIR_UMA,model, models[model]["name"]+FOR_EXT)
        logging.info('Reading file %s for dev-inputs', model_path)
        # Get the model dictionnary for update
        models[model].update(ude.read(
            model_path,
            spe_key='[in]params',
            des_key='description',
            num_key='parameters_number',
            ))
    for model in ('hardening_type',):
        model_path = os.path.join(paths.DIR_UMA,model, models[model]["name"])
        logging.info('Reading file %s for dev-inputs', model_path)
        models[model].update(ude.read(
            model_path,
            spe_key='[in,out]state_vars',
            des_key='statevars_notes',
            num_key='state_vars_number',
            ))
    return models

##########################################################################################
# FILE GENERATION
##########################################################################################

def generate_umat(models:dict, **kva):
    """Generate UMAT and write it to FILE.f

    Notes
    -----
    Lines will not be read of the markup "<PUGIgnoreBegin>"
    appears in the line.
    The reading will resume after "<PUGIgnoreEnd>" appears alone on
    another line.
    """
    kva.setdefault('style', 'relative')# "monolithic"
    kva.setdefault('write_file', True)
    kva.setdefault('open_mode', 'x')

    #------------------------------------------------------------------
    # READ BASE
    txt = ''
    umb_path = os.path.join(paths.DIR_UMA, 'umat_base.f')
    with open(umb_path, encoding='utf8') as file:
        do_read = True
        for line in file:
            if "<PUGIgnoreBegin>" in line:
                do_read = False
            elif "<PUGIgnoreEnd>" in line:
                do_read = True
            elif do_read:
                txt += line

    # Prepare lines to write at placeholders, using `PROJECT_LINES`
    line_rep = copy.deepcopy(PROJECT_LINES)

    line_rep["doxygen file info"] = PROJECT_LINES["doxygen file info"].format(
            "C> ## Project reference: "+models['name']+"\n"+
            "C> The comment are formatted as DOXYGEN docstrings.\n"+
            "C> in order to generate automatic documentation."
            )
    line_rep["props splitting"] = PROJECT_LINES["props splitting"].format(
            models['elasticity']['parameters_number'],
            models['reference_stress']['parameters_number'],
            models['equivalent_stress']['parameters_number'],
            models['hardening_type']['parameters_number'],
            )
    # Inclusion or file copying: the default is to write includes
    if kva['style'] == 'relative':
        line_rep["files inclusions"] = PROJECT_LINES["files inclusions"].format(
            paths.DIR_UMA,
            models['elasticity']['name'],
            models['hardening_type']['name'],
            models['equivalent_stress']['name'],
            models['reference_stress']['name'],
            )
    elif kva['style'] == 'monolithic':
        for model in ('elasticity',
                      'hardening_type',
                      'equivalent_stress',
                      'reference_stress',):
            model_path = os.path.join(paths.DIR_UMA, model, models[model]["name"])
            txt += '\n'*3
            txt += 'C'*72+'\n'*2
            txt += 'C '+model.upper()+': '+models[model]["name"]+'\n'*2
            txt += 'C'*72+'\n'*2
            with open(model_path, encoding='utf8') as modtxt:
                for line in modtxt:
                    txt += line
        line_rep["files inclusions"] = ''
    else:
        raise Exception('Wrong generation style: '+str(kva['style']))


    #------------------------------------------------------------------
    # GENERATE TEXT
    uma_txt = 'C '+client.__licence__.replace('\n', '\nC ')+'\n'
    uma_txt += replace_from_base(txt, line_rep)

    #------------------------------------------------------------------
    # WRITE FILE or return string
    if not kva['write_file']:
        return uma_txt

    with open(UMA_NAME, kva['open_mode'], encoding='utf8') as umfile:
        umfile.write(uma_txt)
    return 0


def generate_inp(models:dict):# pylint: disable=too-many-locals
    """Turn model  description into INP filepart using prescribed
    parameter table md style.
    """
    # BUILD OTHER DICTS -----------------------------------------------
    dict_mat = dict(parameters=dict(name='User'),data=[])
    dict_den = dict(parameters={},data=[2.7e-09,])

    # BUILD STATEV dict -----------------------------------------------
    nsv = models['hardening_type']['state_vars_number']
    sv0 = ude.extract_from_md_table(models['hardening_type']['statevars_notes'], 0)
    sv1 = ude.extract_from_md_table(models['hardening_type']['statevars_notes'], 1)
    datastring = "\n{:8d},".format(nsv)
    for idx in range(nsv):
        datastring +='\n'
        datastring +='{:2d},{:10},"{}"'.format(idx+1, sv0[idx], sv1[idx])
    dict_sva = dict(
            parameters={},
            data=datastring,
            )

    # BUILD USERMAT dict ----------------------------------------------
    comstr = ''
    params = []
    npa = 0
    # Concatenate infos from all sources
    for model in ('elasticity',
                  'reference_stress',
                  'equivalent_stress',
                  'hardening_type',
                  ):
        comstr += model.capitalize()+'\n'
        comstr += models[model]['description']
        params += ude.extract_from_md_table(models[model]['description'],3)
        npa += models[model]['parameters_number']
    comstr = comstr.replace('\n', '\n** ')
    #Build the data by lines of 8
    datastr =''
    for lineno in range(npa//8):
        datastr += '\n '
        datastr += ("{:9},"*8).format(*params[8*lineno:8*lineno+8])
    rest = npa%8
    if rest:
        datastring += ("{:e}"*rest).format(params[-rest:])
    dict_usm = dict(parameters={'constants':npa},
                    #data=params,
                    data=datastr,
                    comments=comstr,
                    )

    # Start writing
    uin.write('Material',      dict_mat, 'KW', file_name=INP_NAME, open_mode='w')
    uin.write('Density',       dict_den, 'KW', file_name=INP_NAME, open_mode='a')
    uin.write('Depvar',        dict_sva, 'KW', file_name=INP_NAME, open_mode='a')
    uin.write('User Material', dict_usm, 'KW', file_name=INP_NAME, open_mode='a')


#**
#** MATERIALS
#**
#*Material, name=User
#*Density
# 2.7e-09,
#*Depvar
#      7,
# 1,ep,    "Equivalent plastic strain"
# 2,epdot, "Equivalent plastic strain rate"
# 3,R,     "Isotropic hardening R1"
# 4,iter,  "Number of Newton iterations in return-map"
# 5,jmax,  "Max number of line-searches used per Newton iter"
# 6,INFO,  "Convergence info"
# 7,resid, "Residual value"
#*User Material, constants=24
#**    YOUNG,    POISS,       s0,     Rsat,    esatm,   YLDEXP,  L1-1122, L1-1133,
#     7.e+04,      0.3,       20,      150,      0.5,        8,      0.0,     0.0,
#**  L1-2222,  L1-2233,  L1-3322,  L1-3333,  L1-4444,  L1-5555,  L1-6666  L2-1122,
#     0.7305,   0.1342,   0.1342,   0.7355,    0.922,    0.637,    0.901,     0.0,
#**	L2-1133,  L2-2222,  L2-2233,  L2-3322,  L2-3333,  L2-4444,  L2-5555  L2-6666
#        0.0,   0.7305,   0.1342,   0.1342,   0.7355,    0.922,    0.637,   0.901
#** ----------------------------------------------------------------

##########################################################################################

if __name__ == "__main__":
    print('-------------------------------')
    print('--- TEMPORARY ---- ------------')
    print('------------- TEST ------------')
    print('------------- ---- FUNCTION ---')
    print('-------------------------------')
    logging.getLogger().setLevel(logging.DEBUG)
    CFG_NAME = client.DEFAULT_NAME+'.yaml'
    mod = cfgfile_to_dict(os.path.join(os.path.dirname(__file__), CFG_NAME))
    generate_umat(mod, open_mode='w', style='relative')
