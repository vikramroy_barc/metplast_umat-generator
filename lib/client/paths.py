"""This module defines paths and interacts with the directory structure
"""
import os
import logging
import yaml

#DIR_CFG = ''#the output directory

DIR_CUR = os.path.abspath(os.getcwd())
DIR_LIB = os.path.relpath(
        os.path.join(os.path.dirname(__file__), ".."),
        DIR_CUR
        )
DIR_UMA = os.path.join(DIR_LIB, "umat/")


logging.info('`path` module called from: %s', DIR_CUR)
logging.info('Library located at: %s', DIR_LIB)

def exists(filepath):
    """Literally an alias to os.path.exists

    Shameless.
    """
    return os.path.exists(filepath)

def scan_umat_options(**kwopt):
    """Return the list of possibilities  for umat models
    Directly from the directory structure.

    Parameters
    ----------
    kwopt : dict
        'return_type'= `dict` (default) or `str`

    Returns
    -------
    RET : str or dict
        The yaml-formatted str or corresponding dict or answers
    """
    logging.info('Scanning the umat directory')
    kwopt.setdefault('return_type', dict)
    umat_sublist = next(os.walk(os.path.join(DIR_UMA, '')))[1] # catch names under ./umat
    tree_dict = dict()
    #[tree_dict.update({key:tuple()}) for key in umat_sublist]

    for dirname in umat_sublist:
        tree_dict[dirname] = list(map(
            lambda st: st.split('.f')[0],
            os.listdir(os.path.join(DIR_UMA, dirname))
            ))

    if kwopt['return_type'] == dict:
        return tree_dict

    if kwopt['return_type'] == str:
        return yaml.dump(tree_dict)

    raise TypeError(str(kwopt['return_type']))

if __name__ == "__main__":
    print('Exec from: '+DIR_CUR)
    print('Libraries: '+DIR_LIB)
    print('Umats at: '+DIR_UMA)

    print('\nScan umat directory:')
    print(scan_umat_options(return_type=str))
