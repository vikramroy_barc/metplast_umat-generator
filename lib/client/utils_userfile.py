"""Utilities for the Umat Generator project: user file.

This module contains parsing tools for the user database.
From this file we expect to get and write project parameters (models, algorithm parameters, etc.).
"""

##pylint: disable=wrong-import-position
import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__),'../')))
import logging
import yaml
import client

DEFAULT_USER = dict(
        name='DEFAULT',
        elasticity={'name':'isotropic'},
        equivalent_stress={'name':'yld2004',},
        reference_stress={'name':'voce',},
        hardening_type={'name':'isotropic',},
        #option1=None,
        )

CFG_NAME = client.DEFAULT_NAME+'.yaml'
UMA_NAME = client.DEFAULT_NAME+'.f'
####################################################################################################
##  WRITING a user config file (yaml)                                                             ##
####################################################################################################

def write(usr_dict, **kwargs):
    r"""Write userfile.yaml from dict

    Parameters
    ----------
    usr_dict : dict
        A dictionary holding project parameters with format defined

    kwargs : dict
        Series of <key=val> arguments. The expected ones ones are
        - file_name:str, the file to write on;
        - open_mode='x', parameters passed to the `open` function.
        - header='PUG\nconfiguration', prepended comments
    """

    kwargs.setdefault('open_mode', 'x')
    kwargs.setdefault('file_name', CFG_NAME)
    kwargs.setdefault('header', client.DEFAULT_NAME+'\nconfiguration')

    logging.info('Writing user project parameters to file %s', kwargs['file_name'])

    check_dict(usr_dict)

    # Read dict
    with open(kwargs['file_name'], kwargs['open_mode'], encoding='utf8') as yamlfile:
        yamlfile.write('# '+kwargs['header'].replace('\n', '\n# '))
        yamlfile.write('\n---\n')
    with open(kwargs['file_name'], 'a', encoding='utf8') as yamlfile:
        yaml.dump(usr_dict, yamlfile, sort_keys=True, indent=2)

####################################################################################################
##  READING a user config file (yaml)                                                             ##
####################################################################################################

def read(file_path:str):
    """Process a USER file expecting a yaml format

    The corresponding dictionary must have keys identical to the reference (`DEFAULT_USER`)

    Parameters
    ----------
    file_path : str
        path to the yaml files to read

    Returns
    -------
    contents : dict
        the dictionnary of project parameters

    """

    logging.info('Reading user project parameters from file %s', file_path)

    # Get the yaml data
    with open(file_path, encoding='utf8') as lefile:
        contents = yaml.load(lefile, Loader=yaml.FullLoader)

    # Make sure the keys are right
    check_dict(contents)

    return contents

def check_dict(usr_dict):
    """Compare a given instance to the reference.
    """
    for key, val in usr_dict.items():
        #print(key+'='+str(val))
        if key not in DEFAULT_USER.keys():
            raise KeyError(key)
        if isinstance(val, dict):
            #print('value is dict: further key check')
            if val.keys() != DEFAULT_USER[key].keys():
                raise KeyError('For dict under '+key)
