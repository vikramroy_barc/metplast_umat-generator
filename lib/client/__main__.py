r"""This is the script executed when client is called

See PEP 338 – Executing modules as scripts:
https://peps.python.org/pep-0338/

Use cases

>>> client CONFIGFILE
Read the CONFIGFILE and generate all

>>> client --generate #or -g
Generate default config file with default name

>>> client -g project.cfg
Same but we choose the name

>>> client --list-opt #or -l
Print the config file possibilities to stdout

>>> client --help #or -h
Print help

>>> client
Print warning

"""
import logging
import os
import sys
import argparse

sys.path.insert( 0, os.path.abspath( os.path.join(os.path.dirname(__file__),'..')))
from client import utils_userfile as uus #pylint: disable=wrong-import-position
from client import paths                 #pylint: disable=wrong-import-position
from client import parser                #pylint: disable=wrong-import-position
from client import DEFAULT_NAME          #pylint: disable=wrong-import-position

UMA_NAME = DEFAULT_NAME+'.f'
CFG_NAME = DEFAULT_NAME+'.yaml'



##########################################################################################
## ARGUMENTS definitions
##########################################################################################

PARSER = argparse.ArgumentParser(
   "client",
   description='''
        The Umat Generator CLI
        long text is very very long lon tim
        ...
        long text is very very long lon tim
        ''',
   )

PARSER.add_argument('cfg_file',
        type=str,
        default=None,
        help='project configuration file path (default: '+CFG_NAME+')',
        nargs='?', # for one arg or default val if no arg
        )
PARSER.add_argument('--generate', '-g',
        const=True, #then we'll use if ARGS.generate
        default=False,
        action='store_const',
        help='generate a default configuration file',
        )
PARSER.add_argument('--list', '-l',
        const=True, #then we'll use if ARGS.list
        default=False,
        action='store_const',
        help='list some available options to write a config file \
                (incompatible with any other argument)',
        )
PARSER.add_argument('--export', '-e',
        const=True, #then we'll use if ARGS.list
        default=False,
        action='store_const',
        help='create a monolithic UMAT file containg all definitions, \
              instead of a small file with relative inclusions \
              (incompatible with other options)',
        )
PARSER.add_argument('--verbose', '-v',
        const=True, #then we'll use if ARGS.list
        default=False,
        action='store_const',
        help='print all logs to stderr (including debug and info levels)',
        )

ARGS = PARSER.parse_args()



##########################################################################################
## FUNCTIONS definitions
##########################################################################################

def generate_cfg(cfg_name):
    """Write the default project data to the config-file (yaml)
    """
    uus.write(uus.DEFAULT_USER,
            file_name=cfg_name,
            open_mode='x',
            )

##########################################################################################
## ARGUMENTS directions
##########################################################################################

# Define logging level for verbose
if ARGS.verbose:
    logging.getLogger().setLevel(logging.DEBUG)

# Throw exceptions when options make no sense
if ARGS.export +  ARGS.generate +  ARGS.list > 1:
    raise Exception('incompatible options')

# Distribute actions
if ARGS.list:
    logging.info('Action: LIST')
    # return str from client.directory.scan
elif ARGS.generate:
    # Assign default value
    cfg_file = ARGS.cfg_file if ARGS.cfg_file else CFG_NAME
    logging.info('Action: GENERATE')
    if paths.exists(cfg_file):
        logging.error('Config file %s already exists.', cfg_file)
    else:
        generate_cfg(cfg_file)
        logging.info('Configuration file written to %s', cfg_file)
else:
    logging.info('Action: bind Umat')
    if not ARGS.cfg_file:
        logging.error('Config file needed for binding Umat')
    else: # Prepare Dict and move to dir
        project = parser.cfgfile_to_dict(ARGS.cfg_file)
        #from pprint import pp #pp(project)
        #import yaml #print(yaml.dump(project))
        #pwd = os.path.abspath(os.curdir)
        #os.chdir(os.path.dirname(os.path.abspath(ARGS.cfg_file)))
        if not ARGS.export:
            logging.debug('Relative includes: append fortran path from within FILE.f?')
            parser.generate_umat(project, style='relative')
        else:
            logging.debug('Absolute include: monolithic file requested.')
            parser.generate_umat(project, style='monolithic')
        parser.generate_inp(project)
        #os.chdir(pwd)


##########################################################################################
## PATH finders
##########################################################################################
