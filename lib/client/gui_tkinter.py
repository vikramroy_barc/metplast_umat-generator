#pylint: disable=invalid-name
"""Simple Graphical Interface for the command line tool

https://docs.python.org/3/library/tkinter.html
"""
#import tkinter #as tk
#from tkinter import Tk, ttk, StringVar
import tkinter as tk
import sys
import os
import yaml

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__),'../')))

#pylint: disable=wrong-import-position
import client.paths
#import client.utils_inp      as uin
#import client.utils_devfile  as ude
import client.utils_userfile as uus
from client import parser

CFGNAME = client.DEFAULT_NAME+'.yaml'
INPNAME = client.DEFAULT_NAME+'.f'
UMTNAME = client.DEFAULT_NAME+'.inp'

def cleanup_dir():
    """Remove CFG and Umat files"""
    for file_path in (UMTNAME, INPNAME, CFGNAME,):
        if os.path.isfile(file_path):
            os.remove(file_path)

class UmatApp(tk.Tk):
    """Main interactive window to create UMAT"""
    def __init__(self):
        tk.Tk.__init__(self)
        #self.geometry("800x480")

        self.make_frames()
        self.make_widgets()


    def make_widgets(self):
        """Creat variables, buttons and distribute them in the frame."""

        # BUILD VARIABLES
        self.cfg_dict = uus.DEFAULT_USER

        opts = client.paths.scan_umat_options(return_type=dict)
        self.vars = dict(
                project_name=tk.StringVar(self),
                elasticity=tk.StringVar(self),
                hardening_type=tk.StringVar(self),
                equivalent_stress=tk.StringVar(self),
                reference_stress=tk.StringVar(self),
                cfg_preview=tk.StringVar(self),
                parser_style=tk.StringVar(self),
                )
        # User choices:
        self.vars['project_name'].set('none')
        self.vars['parser_style'].set('relative')
        self.vars['elasticity'].set(opts['elasticity'][0])
        self.vars['hardening_type'].set(opts['hardening_type'][0])
        self.vars['equivalent_stress'].set(opts['equivalent_stress'][0])
        self.vars['reference_stress'].set(opts['reference_stress'][0])
        # Propagate choices to variables
        self.vars_update()

        # ----------------------------------------------------------------------
        label_style = dict(bg='black', fg='white', width=50,)
        tk.Label(self.frames['title'], text='Plasticity Umat Generator',
                **label_style,
                ).pack(fill='x', side='top', ipadx=150, ipady=15)

        tk.Label(self.frames['title'],
                text='Current directory:\n'+os.getcwd()+'\n',
                **label_style, justify=tk.LEFT,
                ).pack(fill='x')


        # ----------------------------------------------------------------------
        # ASSIGN BUTTONS
        label_style = dict( anchor='w', )#bg='blue',)
        buton_pack = dict(fill='x', anchor=tk.W, padx=20,)

        tk.Label(self.frames['choices'], text='Project name', **label_style).pack(fill='x')
        tk.Entry(self.frames['choices'],
                textvariable=self.vars['project_name']
                ).pack(**buton_pack)
        tk.Label(self.frames['choices'], text='\nElastic model', **label_style
                ).pack(fill='x')
        tk.OptionMenu(self.frames['choices'],
                self.vars['elasticity'], *opts['elasticity']
                ).pack(**buton_pack)
        tk.Label(self.frames['choices'], text='\nYield function type:', **label_style
                ).pack(fill='x')
        tk.OptionMenu(self.frames['choices'],
                self.vars['equivalent_stress'], *opts['equivalent_stress']
                ).pack(**buton_pack)
        tk.Label(self.frames['choices'], text='\nReference stress:', **label_style).pack(fill='x')
        tk.OptionMenu(self.frames['choices'],
                self.vars['reference_stress'], *opts['reference_stress']
                ).pack(**buton_pack)
        tk.Label(self.frames['choices'], text='\nHardening type:', **label_style).pack(fill='x')
        tk.OptionMenu(self.frames['choices'],
                self.vars['hardening_type'],
                *opts['hardening_type'],
                ).pack(**buton_pack)

        # ----------------------------------------------------------------------
        tk.Label(self.frames['choices'], text='\nParser style', **label_style).pack(fill='x')
        monolit = ['relative', 'monolithic']
        radiolab = ['use relative inclusions', 'monolithic file (for export)',]
        for i in range(2):
            tk.Radiobutton(self.frames['choices'],
                    variable=self.vars['parser_style'],
                    text=radiolab[i], value=monolit[i]
                    ).pack(expand=1,anchor=tk.W)

        # ----------------------------------------------------------------------
        tk.Label(self.frames['preview'],
                text='Current configuration:',
                justify=tk.LEFT,
                ).pack(ipadx=10, ipady=10, )
        tk.Label(self.frames['preview'],
                textvariable=self.vars['cfg_preview'],
                bg='gray',font='TkFixedFont', justify=tk.LEFT,
                ).pack(ipadx=10, ipady=10, padx=50)

        # ----------------------------------------------------------------------

        buton_pack = dict(fill='both', side=tk.LEFT, anchor=tk.W, padx=20,ipadx=20, ipady=20,)
        tk.Button(self.frames['actions'], text="Apply",
                command=self.vars_update).pack(**buton_pack)
        tk.Button(self.frames['actions'], text="Generate",
                wraplength=100, command=self.umake).pack(**buton_pack)
        tk.Button(self.frames['actions'],text="Quit",
                command=self.quit).pack(**buton_pack)


        # ----------------------------------------------------------------------
        tk.Label(self.frames['general'],
                text='''NOTES: add references, gitlab page, ...?''',
                justify=tk.LEFT,
                ).pack()

    def vars_update(self):
        """ Propagate user choices to variables.

        When press "Apply", the user choices are assigned to the config-dictionary.
        Then the displayed config-string is updated accordingly.
        """

        # Assign StringVar to cfg dict
        self.cfg_dict['name']                      = self.vars['project_name'].get()
        self.cfg_dict['elasticity']['name']    = self.vars['elasticity'].get()
        self.cfg_dict['equivalent_stress']['name'] = self.vars['equivalent_stress'].get()
        self.cfg_dict['reference_stress']['name']    = self.vars['reference_stress'].get()
        self.cfg_dict['hardening_type']['name']    = self.vars['hardening_type'].get()

        # Update CFG string accordingly
        self.vars["cfg_preview"].set(
                'Uat\nconfiguration\n'+
                '(style: {})'.format(self.vars['parser_style'].get().upper())+
                '\n---\n'+
                yaml.dump(self.cfg_dict, indent=4)
                )


    def umake(self):
        """When "Generate" is pressed, then we write the dictionary to a yaml config file
        and use it to make FILE.f"""
        cleanup_dir()
        uus.write(self.cfg_dict, open_mode='w', file_name=CFGNAME)
        project = parser.cfgfile_to_dict(CFGNAME)
        os.remove(CFGNAME)
        parser.generate_umat(project, style=self.vars['parser_style'].get())
        parser.generate_inp(project)

    def make_frames(self):
        """Divide the main window into several frames
        and return them in a dictionary.

        --------------------------------
        |   TITLE and directory info   |
        --------------------------------
        |              |               |
        |              |               |
        |   USER       |  CFG PREVIEW  |
        |   CHOICES    |               |
        |              |               |
        |------------------------------|
        |                              |
        |         Main actions         |
        |                              |
        |------------------------------|
        |   Notes, credits, comments.  |
        --------------------------------


        Defines
        -------
        self.frames : dict of frames
        """
        # pylint: disable=multiple-statements
        fTOP = tk.Frame(self); fTOP.pack(pady=20, side=tk.TOP, )

        ftop = tk.Frame(self); ftop.pack(pady=20, side=tk.TOP, )
        fmid = tk.Frame(self); fmid.pack(pady=20,)
        fbot = tk.Frame(self, background='pink'); fbot.pack(pady=20, side=tk.BOTTOM)

        ftopleft = tk.Frame(ftop); ftopleft.pack(side=tk.LEFT)
        ftopright = tk.Frame(ftop); ftopright.pack(side=tk.RIGHT, expand=True)

        self.frames = dict(
                title=fTOP,
                choices=ftopleft,
                actions=fmid,
                preview=ftopright,
                general=fbot,
                )

if __name__ == "__main__":
    app = UmatApp()
    app.title("PUG-GUI-tk")
    app.mainloop()
