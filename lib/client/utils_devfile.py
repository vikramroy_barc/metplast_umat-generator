"""Utilities for the Umat Generator: development files.

This module provides parsing tools for development files.
That is the different Fortran files where we expect additional
information.

We make use of DOXYGEN doctstring to simplify the developper's job.

"""

import logging
import fileinput
#import copy
import os

import client.utils_inp      as uin

####################################################################################################
## READING a umat/fortran special header
####################################################################################################

USER_PREFIX = 'C* '
USER_PREFIX = USER_PREFIX.lower()
UP_LEN = len(USER_PREFIX)

KEYW_PREFIX = '#'
KW_LEN = len(KEYW_PREFIX)


DEFAULT_MOD = dict(
        name='',
        description='',
        parameters_number=0,
        comments='',
        )

####################################################################################################
####################################################################################################


def read(file_path:str, **kva):
    """Read parameters number and description on top of a Doxygen fortran docstring.

    Parameters
    ----------
    file_path : str
        Path to the fortran file that docstring shall be scanned

    Other parameters
    -----------------
    **kva : dict
        spe_key : special keyword to look for in doxygen docstrings.
        num_key : where to write the number found under parenthesis after spe_key
        des_key : where to write the following description


    Notes
    -----
    The expected pattern  is as follows

        C> @file filename.f
        C> ...
        C> @param[in] params(<NUMBER>)
        C>    Some lines
        C>
        C>    |Symbol | Name          | Unit  | eg |
        C>    |-------|---------------|-------|----|
        C>    | L_11  | coeff         | -     | 1. |
        C>    | L_22  | coeff         | -     | 1. |
        C>    | a     | exponent      | -     | 8. |
        C>
        C> @param state_vars (1)
        C>    Some lines
        C>
        C>    |Symbol | Name            | Unit  |
        C>    |-------|---------------  |-------|
        C>    | aps   | acc.plas. str.  | -     |
        C>
        C>    This is ignored.

    This way we can catch the number of parameters and will append every markdown table line
    To the "description" of the returned model
    """
    kva.setdefault('spe_key', '[in]params')
    kva.setdefault('des_key', 'description')
    kva.setdefault('num_key', 'parameters_number')

    model={'name':os.path.basename(file_path),
            kva['des_key']:'',
            kva['num_key']:0,
            }
    logging.info('Reading special doc line from file %s', file_path)

    with fileinput.input(files=(file_path)) as le_file:
        seek_md = False
        for line in le_file:
            # Append description if KEY has been read
            if seek_md and line.lower().startswith('c>    |'):
                model[kva['des_key']] += line[4:]
            # Otherwise look for KEY
            elif line.replace(' ', '').lower().startswith(
                    ('c>@param'+kva['spe_key']).replace(' ', '').lower()
                    ):
                model[kva['num_key']] = int(line[line.find('(')+1:line.find(')')])
                seek_md = True
            # Stop MD reading when meeting another keyword
            elif line.replace(' ', '').lower().startswith('c>@'):
                # Stop reading at next keyword
                seek_md = False
    #if model(kva['des_key']

    return model


def extract_from_md_table(md_string, col_index=0):
    """Given a markdown-formatted table, return a list of values found in the given column
    """
    vals=list(
            map(lambda x: uin.parse_val(     #num if necessary
                x.split('|')[col_index+1]# catch column index
                ),
                md_string.split('\n')[2:-1]# ignore 2 first and last
                )
            )
    return vals

#def extract_default_params(mod_dict:dict):
#    """Given a dev-file model, read the default values from
#    the description (markdown table).
#
#    Arguments
#    ---------
#    mod_dict : dict
#        A model dictionary holding at least:
#        - 'description': the string containing a markdown-formatted table
#          giving *default parameter values in column 4.*
#          The colunm title does not matter, only column FOUR.
#        - 'parameters_number' which must match the table length.
#          (the two first rows, title+rule, are excluded)
#
#    Returns
#    -------
#    params : list
#        list of default parameters
#    """
#    params = [None]*mod_dict['parameters_number']
#    for idx, val in enumerate(extract_from_md_table(mod_dict['description'], 3)):
#        params[idx] = val
#
#    return params
#
#
#def _read_old_version(file_path:str):
#    """Return the dictionary of developer information for UMAT/Fortran files.
#
#    Arguments
#    ---------
#    file_path : str
#        The path to the Fortran file to scan.
#
#    Returns
#    -------
#    model : the dict of parameters
#
#    Notes
#    -----
#    If a keyword is invalid, an exception is thrown.
#    If a keyword is repeated, the value is replaced if integer
#    or appended if string.
#    """
#    logging.info('Reading project headers from file %s', file_path)
#    model=copy.deepcopy(DEFAULT_MOD)
#    last_key='comments'
#    with fileinput.input(files=(file_path)) as le_file:
#        for line in le_file:
#
#            # Do smth only if USER_PREFIXED
#            if line.lower().startswith(USER_PREFIX):
#                # Remove user prefix
#                line = line[UP_LEN:].strip()
#
#                # Update keyword if needed
#                if line.startswith(KEYW_PREFIX):
#                    last_key = line.split()[0][KW_LEN:].lower()
#                    line = line[len(last_key)+KW_LEN:]
#
#                # Check
#                if last_key not in DEFAULT_MOD.keys():
#                    raise KeyError(last_key)
#
#                # Make int if needed, then append
#                if not bool(line):
#                    pass
#                elif last_key == 'parameters_number':
#                    model[last_key] += int(line)
#                elif last_key == 'name':
#                    model['name'] += line.strip()
#                else:
#                    model[last_key] += '\n'+line.strip() if model[last_key] else line.strip()
#
#        # Make sure name was given at least
#        if len(model["name"]) == 0:
#            raise Exception("Minimum requirement: #name; err in file", file_path)
#
#    return model

####################################################################################################
####################################################################################################

def check_dict(mod):
    """Compare a given instance to the reference.
    """
    for key in mod.keys():
        #print(key+'='+str(val))
        if key not in DEFAULT_MOD.keys():
            raise KeyError(key)
