"""Python client unit testing

From this script's path:
>>> pytest -v # scans for appropriate files anyways

The strategy here is to define file contents examples, write and read them
to check if the reader does the expected job.
"""

# pylint: disable=protected-access

import os
import pytest

from context import utils_devfile as udv

from context import log_new_test
log_new_test(os.path.basename(__file__).upper())

####################################################################################################
# PREPARATION

FILE_NAME = 'sample.f'

def writetest(filestring):
    """Write an example string to the test file. Erases previous contents."""
    with open(FILE_NAME, 'w', encoding='utf8') as le_file:
        le_file.write(filestring)


####################################################################################################
# TESTS WRITING

def test_writetest():
    """Quick writing test"""
    with pytest.raises(Exception) as excinfo:
        writetest(1)
    assert excinfo.errisinstance(TypeError)


####################################################################################################
# TESTS DOXYGEN READER

def test_doxygen_01():
    """The reading process goes fine"""
    writetest(DO1)
    mod = udv.read(FILE_NAME)
    assert mod["name"] == FILE_NAME
    assert mod["parameters_number"] == 19

DO1=r"""
C-----------------------------------------------------------------------
C> @author
C>    - Thomas Manik: writing core, algorithm, concept.
C>    - Baptiste Reyne: refactoring for doxygen and umeta.
C-----------------------------------------------------------------------
C> @brief 
C>    Calculates Barlat's Yld2004 yield function and/or its gradient
C>    and/or its Hessian expressed in the natural notation.
C> @todo details with formulation and reference to paper
C-----------------------------------------------------------------------
C> @param[in] SVEC stress tensor given in the natural notation
C> @param[in] PARAMS(19) The anisotropic parameters of 
C>    Yld2004 expected in the natural notation. 

C>    |Symbol | Name          | Unit  | DEFVAL|
C>    |-------|---------------|-------|-------|
C>    | L'_12 | coeff         | -     | 12.11 |
C>    | L'_13 | coeff         | -     | 12.12 |
C>    | L'_22 | coeff         | -     | 12.13 |
C>    | L'_23 | coeff         | -     | 12.14 |
C>    | L'_32 | coeff         | -     | 12.15 |
C>    | L'_33 | coeff         | -     | 12.16 |
C>    | L'_44 | coeff         | -     | 12.17 |
C>    | L'_55 | coeff         | -     | 12.18 |
C>    | L'_66 | coeff         | -     | 12.19 |
C>    | L"_12 | coeff         | -     | 12.20 |
C>    | L"_13 | coeff         | -     | 12.21 |
C>    | L"_22 | coeff         | -     | 12.22 |
C>    | L"_23 | coeff         | -     | 12.23 |
C>    | L"_32 | coeff         | -     | 12.24 |
C>    | L"_33 | coeff         | -     | 12.25 |
C>    | L"_44 | coeff         | -     | 12.26 |
C>    | L"_55 | coeff         | -     | 12.27 |
C>    | L"_66 | coeff         | -     | 12.28 |
C>    | a     | exponent      | -     | 12.29 |

C>    The exponent in Yld2004 \p a (must be >= 2)
C>    The first L anisotropic transformation reads
C>    \f[
C>     \begin{bmatrix}
C>      0  & L_{12} & L_{13} &  0     &  0     &  0 \\
C>      0  & L_{22} & L_{23} &  0     &  0     &  0 \\
C>      0  & L_{32} & L_{33} &  0     &  0     &  0 \\
C>      0  &  0     &  0     & L_{44} &  0     &  0 \\
C>      0  &  0     &  0     &  0     & L_{55} &  0 \\
C>      0  &  0     &  0     &  0     &  0     & L_{66}
C>     \end{bmatrix}
C>    \f]

C> @param[in] MODE
C>    Controls which of F, GRAD, HESSIAN is to be calculated
C>    - 0  :  only \p PHI is calculated
C>    - 1  :  only \p PHI and \p GRAD is calculated
C>    - else :  \p PHI, \p GRAD and \p HESSIAN are calculated
C
C-----------------------------------------------------------------------
C> @attention The parameters must be provided in the natural notation.
C-----------------------------------------------------------------------
"""

def test_extract_from_md_table():
    """Check values from D01"""
    writetest(DO1)
    mod = udv.read(FILE_NAME)
    params = udv.extract_from_md_table(mod['description'],3)
    assert len(params) == mod["parameters_number"]
    for idx, val in enumerate(range(1211, 1230)):
        assert params[idx] == (val)/100.



#def test_extract_default():
#    """Check values from D01"""
#    writetest(DO1)
#    mod = udv.read(FILE_NAME)
#    params = udv.extract_default_params(mod)
#    assert len(params) == mod["parameters_number"]
#    for idx, val in enumerate(range(1211, 1230)):
#        assert params[idx] == (val)/100.


def test_doxygen_02():
    """Wrong param number input"""
    writetest(DO2)
    with pytest.raises(Exception) as excinfo:
        _ = udv.read(FILE_NAME)
    assert "invalid literal for int()" in str(excinfo.value)

DO2=r"""
C-----------------------------------------------------------------------
C> @param[in] SVEC the variable
C>    Some docstring here
C> @param[in] PARAMS 19
C>    Some docstring here
"""

def test_doxygen_03():
    """Non default keys"""
    writetest(DO3)
    mod = udv.read(FILE_NAME,
            spe_key='HELLO',
            des_key='NOTES',
            num_key='NUM',
            )
    assert mod["name"] == FILE_NAME
    assert mod["NUM"] == 999
    assert mod["NOTES"] == ''

DO3=r"""
C-----------------------------------------------------------------------
C> @param HELLO (999)
C>    Some docstring here
"""

###############################################################################
## TESTS DEFINITIONS
#
#def test_example_01_basic():
#    """The reading process goes fine"""
#    writetest(EX1)
#    mod = udv._read_old_version(FILE_NAME)
#    assert mod["name"] == 'Some model name'
#    assert mod["parameters_number"] == 5
#
#EX1=r"""
#C**********************************************************************
#C**                       ~ UMETA Header ~                           **
#C**********************************************************************
#C*
#C* #name              Some model name
#C*
#C* #parameters_number 5
#C*
#C* #description       Parameters:
#C*                    ---------------------------------
#C*                    |Symbol | Name          | Unit  |
#C*                    |-------|---------------|-------|
#C*                    |   P1  | Young modulus | Mpa   |
#C*                    |   P2  | Poisson ratio | -     |
#C*                    |   P3  | INP file name | deg K |
#C*                    |   P4  | Perfect match | -     |
#C*                    |   P5  | Curvy letters | rad/s |
#C*                    |   P1  | Columns sides | -     |
#C*                    ---------------------------------
#C*
#C* #comments 0123
#C*
#C**********************************************************************
#
#C This is a normal comment that is ignored
#C> @brief This is the Doxygen description of the following function
#C> This is another Doxygen line
#
#      subroutine square_cube(i, isquare, icube)
#        integer, intent (in)  :: i              ! input
#        integer, intent (out) :: isquare, icube ! output
#
#        isquare = i**2
#        icube   = i**3
#      end subroutine
#
#"""
#
#########################################################################
#
#def test_example_02_multiline_param():
#    """The reading process is predictible"""
#    writetest(EX2)
#    mod = udv._read_old_version(FILE_NAME)
#    assert mod["name"] == 'Von Mises'
#    assert mod["parameters_number"] == 7
#
#EX2="""
#C*************************************************
#C*
#C* #NaMe                                 Von Mises
#C*
#C* #PARAmeters_number                            0
#C* #paraMeters_number                           10
#C*                              -20
#C*                              +17
#C =====================> TOTAL = 7
#C Above we mess with the convention by adding
#C values. This block is not seen because it does
#C not start with the right prefix
#C*************************************************
#"""
#EX2="""
#
#C*
#C* #NaMe                                 Von M
#C*                                            i
#C* #name                                       s
#C*                                              e
#C*                                             s
#C*
#C* #PARAmeters_number             0
#C* #paraMeters_nUMBer            10
#C*                              -20
#C*                              +17
#
#C |=====================> TOTAL = 7
#"""
#########################################################################
#
#def test_example_03_multiline_name():
#    """The reading process is predictible"""
#    writetest(EX3)
#    mod = udv._read_old_version(FILE_NAME)
#    assert mod["name"] == 'Letter by letter'
#
#EX3="""
#C*************************************************
#C*
#C* #NaMe  L
#C*         e
#C*          t
#C*          t
#C*          e
#C*          r by letter
#C*************************************************
#"""
#
###########################################################################
#
#def test_example_04_wrong():
#    """The reading process is predictible"""
#    writetest(EX4)
#    with pytest.raises(Exception) as excinfo:
#        udv._read_old_version(FILE_NAME)
#    assert "Minimum requirement: #name" in str(excinfo.value)
#
#EX4="""
#C* NAme = wrong inpout
#C*
#"""
###########################################################################
# CLEANUP

def test_remove_files():
    """Cleanup dir after writing dummy files"""
    os.remove(FILE_NAME)

############################################################################
#

#test_writetest()
#test_example_01_basic()
#test_example_03_multiline_name()
#test_example_02_multiline_param()
#test_example_04_wrong()
#test_remove_files()
