"""Python client unit testing

From this script's path:
>>> pytest -v # scans for appropriate files anyways
"""

import os
import pytest
from context import utils_inp as uin

from context import log_new_test
log_new_test(os.path.basename(__file__).upper())

####################################################################################################
# PREPARATION
KWNAME = 'USer Material'
KWDAT = [ 7.e+04, 'some string', 50 ]
DICT_EX = {
        'comments':'comment string',
        'parameters': {'constants': 25},
        'data': KWDAT,
        }
FILE_NAME = 'sample.inp'

uin.write(KWNAME, DICT_EX, file_name=FILE_NAME, open_mode='w',)


####################################################################################################
# ELEMENTARY FUNCTIONS

def test_parse_val():
    """Try to write numbers and retrieve their values"""
    for val in ('a', 1, 1e4, 9.98, 100000548745000,):
        assert uin.parse_val(str(val)) == val

####################################################################################################
# WRITING FUNCTIONS

def test_write_on_existing():
    """Try to write on an existing file with default option"""
    with pytest.raises(Exception) as excinfo:
        uin.write('FILE EXISTS', DICT_EX, file_name=FILE_NAME)
    assert 'File exists' in str(excinfo.value)

def test_write_missing_keys():
    """Try to write with missing keyword 'data' or 'parameter'"""
    with pytest.raises(Exception) as excinfo:
        uin.write('wring dada', {'dada':6}, open_mode='w', file_name='other_'+FILE_NAME)
    assert excinfo.errisinstance(KeyError)

####################################################################################################
# READING FUNCTIONS

def test_read():
    """The reading process goes fine"""
    uin.read(FILE_NAME)

def test_read_keyword_name():
    """The written data name identical to the read one"""
    name = uin.read(FILE_NAME)[0][0]
    assert KWNAME.lower() == name.lower()

def test_read_data():
    """The written data is read identical to what we wrote"""
    dat = uin.read(FILE_NAME)[0][1]["data"]
    assert dat == KWDAT

####################################################################################################
# OTHERS

def test_find_keyword():
    """Find the keyword we just wrote: index 0"""
    inp_tup = uin.read(FILE_NAME)
    assert uin.find_keyword(inp_tup, KWNAME) == [0]

def test_select_keyword():
    """Return the list we just wrote, because only a single KW"""
    inp_tup = uin.read(FILE_NAME)
    assert uin.select_keyword(inp_tup, KWNAME) == inp_tup


####################################################################################################
# CLEANUP

def test_remove_files():
    """Cleanup dir after writing dummy files"""
    os.remove(FILE_NAME)
    os.remove('other_'+FILE_NAME)


####################################################################################################
#

#if __name__ == "__main__":
#    uin.write(KWNAME, DICT_EX, file_name=FILE_NAME, open_mode='w',)
#    test_parse_val()
#    test_write_on_existing()
#    test_write_missing_keys()
#    test_read()
#    test_read_keyword_name()
#    test_read_data()
#    test_remove_files()
