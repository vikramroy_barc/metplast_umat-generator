"""Python client unit testing

From this script's path:
>>> pytest -v # scans for appropriate files anyways
"""

import os
import pytest
from context import DEFAULT_NAME
from context import utils_userfile as uus
from context import parser as ump
from context import log_new_test
log_new_test(os.path.basename(__file__).upper())
#log_new_test('PARSER')


####################################################################################################
# PREPARATION
FILLER = """
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Aliquam ac lacus et dolor finibus convallis at quis ante.
Nam sed scelerisque est. Nam auctor semper mollis. Fusce et mollis ipsum, ac hendrerit massa.
Quisque semper eros faucibus enim elementum, in laoreet magna molestie.
""".strip()
PH = "C> This\nC> is place holder: {}.\nC> -----------------\n"
PH=ump.PHS
BASE= ''
REF= ''
REPLACE = dict()
KW_LIST = ('first', 'second', 'third',)
for kword in KW_LIST:
    BASE += FILLER+'\n'+PH.format(kword)
    REF += FILLER+'\n'+kword*2+'\n'
    REPLACE[kword]=kword*2+'\n'

# Generate default CONFIG
CFG = DEFAULT_NAME+'.yaml'
uus.write(uus.DEFAULT_USER, file_name=CFG, open_mode='w')


####################################################################################################
# STRING MANAGEMENT

def test_replace_from_base():
    """Compare predicted and computed texts"""
    assert ump.replace_from_base(BASE,REPLACE) == REF

def test_cfg2dict_normal():
    """Read default dict values in the default cfg file !"""
    proj = ump.cfgfile_to_dict(CFG)
    for key in ('hardening_type','equivalent_stress','reference_stress',):
        assert proj[key]['name'] == uus.DEFAULT_USER[key]['name']+ump.FOR_EXT

def test_cfg2dict_nofile():
    """there is no file at the provided path"""
    with pytest.raises(Exception) as excinfo:
        ump.cfgfile_to_dict('thisisnotafile.yaml')
    assert "onfig file does not exist" in str(excinfo.value)

def test_gen_umeta():
    """ I don't really know here, let's just generate the file and see?"""
    proj = ump.cfgfile_to_dict(CFG)
    ump.generate_umat(proj, open_mode='w', style='relative')
    ump.generate_umat(proj, open_mode='w', style='monolithic')
    umetxt = ump.generate_umat(proj, write_file=False)
    assert 'Project reference: DEFAULT' in umetxt

def test_gen_umeta_bad_dict():
    """Wrong keys and all"""
    badd = dict(name='wrong', equivStress='Misess')
    with pytest.raises(Exception) as excinfo:
        ump.generate_umat(badd, open_mode='w', style='relative')
    assert excinfo.errisinstance(KeyError)

def test_gen_inp():
    """Call and see what happens?"""
    proj = ump.cfgfile_to_dict(CFG)
    ump.generate_inp(proj)

####################################################################################################
# DO: inp generation, description check?

#def test_todo():
#    """todo"""
#    with pytest.raises(Exception) as excinfo:
#        raise Exception('Todo')
#    assert excinfo.errisinstance(Exception)


####################################################################################################
# CLEANUP

def test_remove_files():
    """Cleanup dir after writing dummy files"""
    os.remove(CFG)
    os.remove(DEFAULT_NAME+'.f')
    os.remove(DEFAULT_NAME+'.inp')

####################################################################################################
#
