C                                                     <PUGIgnoreBegin>
    
C> @file umat_base.f The base file for every project.
C>    Defines core concepts and placeholders that the client replaces.
C>    This description will not be copied to the exported files.

C                                                       <PUGIgnoreEnd>
C ======================================================================

C=======================================================================
C            _                     _           _     _             
C           | |                   | |         | |   | |            
C      ____ | | ____  ____ ____   | | _   ___ | | _ | | ____  ____ 
C     |  _ \| |/ _  |/ ___/ _  )  | || \ / _ \| |/ || |/ _  )/ ___)(_)
C     | | | | ( ( | ( (__( (/ /   | | | | |_| | ( (_| ( (/ /| |    
C     | ||_/|_|\_||_|\____\____)  |_| |_|\___/|_|\____|\____|_|    (_)
C     |_|                                                          
C     ------------------------------------------------------------------
C     doxygen file info
C=======================================================================

C ======================================================================
C> @brief The parameters detailed here are copied from
C>    <a href="https://abaqus-docs.mit.edu/2017/English/SIMACAESUBRefMap/simasub-c-umat.htm#simasub-c-umat">
C>     the online documentation
C>    </a>.
C>
C> @details The subroutine \p umat and its requirement are defined and
C>    imposed by Simulia/Abaqus conventions.
C ----------------------------------------------------------------------

CC> ##### Must be computed By the umat

C> @param[out] DDSDDE <b>(NTENS,NTENS)</b>
C>    Jacobian matrix of the constitutive model, ∂Δσ/∂Δε, where Δσ are
C>    the stress increments and Δε  are the strain increments
C>    DDSDDE(I,J) defines  the change in the I-th stress
C>    component at the end of the time increment caused by an
C>    infinitesimal perturbation of the J-th component of the strain
C>    increment array unless you invoke the unsymmetric equation
C>    solution capability for the user-defined material, 
C>    Abaqus/Standard will use only the symmetric part of DDSDDE
C> @param[out] STRESS <b>(NTENS)</b>
C>    This array is passed in as the Cauchy stress tensor at the
C>    beginning of the increment and must be updated in this routine to
C>    be the stress tensor at the end of the increment. In finite-strain
C>    problems the stress tensor has already been rotated to account for
C>    rigid body motion in the increment before UMAT is called, so that
C>    only the corotational part of the stress integration should be
C>    done
C>    in UMAT
C> @param[out] STATEV <b>(NSTATV)</b>
C>    An array containing the SDV is passed in as the values at the
C>    beginning of the increment and must be returned as the values
C>    at  the end of the increment.<br />In finite-strain problems
C>    any vector-valued or tensor-valued state variables must be rotated
C>    to account for rigid body motion of the material, in addition to
C>    any update in the values associated with constitutive behavior.
C>    The rotation increment matrix, DROT, is provided for this purpose.
C> @param[in,out] SSE
C>    Specific elastic strain energy.
C> @param[in,out] SPD
C>    Plastic dissipation.
C> @param[in,out] SCD
C>    Creep dissipation.

C> ##### Only in a fully coupled thermal-stress or a coupled thermal-electrical-structural analysis 
                                                  
C> @param rpl
C>    Volumetric heat generation per unit time at the end of the
C>    increment caused by mechanical working of the material. 
C> @param ddsddt <b>(NTENS)</b>
C>    Variation of the stress increments with respect to the
C>    temperature. 
C> @param drplde <b>(NTENS)</b>
C>    Variation of RPL with respect to the strain increments. 
C> @param drpldt
C>    Variation of RPL with respect to the temperature. 

C> ##### Variables passed in for information

C> @param stran <b>(NTENS)</b>
C>    An array containing the total strains at the beginning of the
C>    increment.
C>    If thermal expansion is included in the same material definition,
C>    the strains passed into UMAT are the mechanical strains only
C>    (that is, the thermal strains computed based upon the thermal
C>    expansion coefficient have been subtracted from the total
C>    strains).
C>    These strains are available for output as the “elastic” strains.
C>    In finite-strain problems the strain components have been rotated
C>    to account for rigid body motion in the increment before UMAT is
C>    called and are approximations to logarithmic strain. 
C> @param dstran <b>(NTENS)</b>
C>    Array of strain increments. If thermal expansion is included in
C>    the same material definition, these are the mechanical strain
C>    increments (the total strain increments minus the thermal strain
C>    increments). 
C> @param time <b>(2)</b>
C>    1. Value of step time at the beginning of the current increment or
C>    frequency. 
C>    2. Value of total time at the beginning of the current increment.
C> @param dtime
C>    Time increment.
C> @param temp
C>    Temperature at the start of the increment. 
C> @param dtemp
C>    Increment of temperature. 
C> @param predef
C>    Array of interpolated values of predefined field variables at this
C>    point at the start of the increment, based on the values read in
C>    at the nodes. 
C> @param dpred
C>    Array of increments of predefined field variables. 
C> @param cmname
C>    User-defined material name, left justified. Some internal material
C>    models are given names starting with the “ABQ_” character string.
C>    To avoid conflict, you should not use “ABQ_” as the leading string
C>    for CMNAME.  
C> @param ndi
C>    Number of direct stress components at this point. 
C> @param nshr
C>    Number of engineering shear stress components at this point. 
C> @param NTENS
C>    Size of the stress or strain component array (NDI + NSHR). 
C> @param NSTATV
C>    Number of solution-dependent state variables that are associated
C>    with this material type (defined as described in Allocating
C>    space).                                                   
C> @param props <b>(nprops)</b>
C>    User-specified array of material constants associated with this
C>    user material. 
C> @param nprops
C>    User-defined number of material constants associated with this
C>    user material.
C> @param coords
C>    An array containing the coordinates of this point. These are the
C>    current coordinates if geometric nonlinearity is accounted for
C>    during the step (see Defining an analysis); otherwise, the array
C>    contains the original coordinates of the point. 
C> @param drot <b>(3,3)</b>
C>    Rotation increment matrix. This matrix represents the increment of
C>    rigid body rotation of the basis system in which the components of
C>    stress (STRESS) and strain (STRAN) are stored. It is provided so
C>    that vector- or tensor-valued state variables can be rotated
C>    appropriately in this subroutine: stress and strain components are
C>    already rotated by this amount before UMAT is called. This matrix
C>    is passed in as a unit matrix for small-displacement analysis and
C>    for large-displacement analysis if the basis system for the
C>    material point rotates with the material (as in a shell element or
C>    when a local orientation is used). 
C> @param pnewdt
C>    Ratio of suggested new time increment to the time increment being
C>    used (DTIME, see discussion later in this section). This variable
C>    allows you to provide input to the automatic time incrementation
C>    algorithms in Abaqus/Standard (if automatic time incrementation is
C>    chosen). For a quasi-static procedure the automatic time stepping
C>    that Abaqus/Standard uses, which is based on techniques for
C>    integrating standard creep laws (see Quasi-static analysis),
C>    cannot be controlled from within the UMAT subroutine.
C>    PNEWDT is set to a large value before each call to UMAT.
C>    If PNEWDT is redefined to be less than 1.0, Abaqus/Standard must
C>    abandon the time increment and attempt it again with a smaller
C>    time increment. The suggested new time increment provided to the
C>    automatic time integration algorithms is PNEWDT × DTIME, where the
C>    PNEWDT used is the minimum value for all calls to user subroutines
C>    that allow redefinition of PNEWDT for this iteration.
C>    If PNEWDT is given a value that is greater than 1.0 for all calls
C>    to user subroutines for this iteration and the increment converges
C>    in this iteration, Abaqus/Standard may increase the time
C>    increment. The suggested new time increment provided to the
C>    automatic time integration algorithms is PNEWDT × DTIME, where the
C>    PNEWDT used is the minimum value for all calls to user subroutines
C>    for this iteration.
C>    If automatic time incrementation is not selected in the analysis
C>    procedure, values of PNEWDT that are greater than 1.0 will be
C>    ignored and values of PNEWDT that are less than 1.0 will cause the
C>    job to terminate. 
C> @param celent
C>    Characteristic element length, which is a typical length of a line
C>    across an element for a first-order element; it is half of the
C>    same typical length for a second-order element. For beams and
C>    trusses it is a characteristic length along the element axis.
C>    For membranes and shells it is a characteristic length in the
C>    reference surface. For axisymmetric elements it is a
C>    characteristic length in the (r,z) plane only.
C>    For cohesive elements it is equal to the constitutive thickness.
C> @param dfgrd0 <b>(3,3)</b>
C>    Array containing the deformation gradient at the beginning of the
C>    increment. If a local orientation is defined at the material
C>    point, the deformation gradient components are expressed in the
C>    local coordinate system defined by the orientation at the
C>    beginning of the increment. For a discussion regarding the
C>    availability of the deformation gradient for various element
C>    types, see Deformation gradient.
C> @param dfgrd1 <b>(3,3)</b>
C>    Array containing the deformation gradient at the end of the
C>    increment. If a local orientation is defined at the material
C>    point, the deformation gradient components are expressed in the
C>    local coordinate system defined by the orientation. This array is
C>    set to the identity matrix if nonlinear geometric effects are not
C>    included in the step definition associated with this increment.
C>    For a discussion regarding the availability of the deformation
C>    gradient for various element types, see Deformation gradient.
C> @param noel
C>    Element number.
C> @param npt
C>    Integration point number. 
C> @param layer
C>    Layer number (for composite shells and layered solids). 
C> @param kspt
C>    Section point number within the current layer.
C> @param jstep  <b>(4)</b>
C>    1. Step number.
C>    2. Procedure type key (see Results file output format).
C>    3. 1 if NLGEOM=YES for the current step; 0 otherwise.
C> @param kinc
C>    Increment number.


C ======================================================================

      subroutine umat(STRESS, STATEV, DDSDDE, SSE, SPD, SCD,
     + rpl, ddsddt, drplde, drpldt,
     + stran, dstran, time, dtime, temp, dtemp, predef, dpred, cmname,
     + ndi, nshr, ntens, nstatv, PROPS, NPROPS, coords, drot, pnewdt,
     + celent, dfgrd0, dfgrd1, noel, npt, layer, kspt, jstep, kinc)

c     implicit none
      include 'aba_param.inc'

      character*80 cmname
      dimension STRESS(NTENS), STATEV(NSTATV),
     + DDSDDE(NTENS, NTENS), DDSDDT(NTENS), DRPLDE(NTENS),
     + STRAN(NTENS), DSTRAN(NTENS), TIME(2), PREDEF(1), DPRED(1),
     + PROPS(NPROPS), COORDS(3), DROT(3,3), DFGRD0(3,3), DFGRD1(3,3),
     + JSTEP(4)

c ---------------------- MAIN DECLARATIONS ----------------------------
      integer     info
      logical     firstinc
      real*8      ovsqrt3, ovsqrt2, ovsqrt6, sqrt2, sqrt3, ov2, ov3
      common /consts/ ovsqrt3, ovsqrt2, ovsqrt6, sqrt2, sqrt3, ov2, ov3
      data   firstinc /.TRUE./

C=======================================================================
C            _                     _           _     _             
C           | |                   | |         | |   | |            
C      ____ | | ____  ____ ____   | | _   ___ | | _ | | ____  ____ 
C     |  _ \| |/ _  |/ ___/ _  )  | || \ / _ \| |/ || |/ _  )/ ___)(_)
C     | | | | ( ( | ( (__( (/ /   | | | | |_| | ( (_| ( (/ /| |    
C     | ||_/|_|\_||_|\____\____)  |_| |_|\___/|_|\____|\____|_|    (_)
C     |_|                                                          
C     ------------------------------------------------------------------
C     props splitting
C=======================================================================

c ---------------------- DEFINING SOME CONSTANTS ----------------------

      ov2 = 0.5d0
      ov3 = 1.d0/3.d0
      ovsqrt3 = 1.d0/sqrt(3.d0)
      ovsqrt2 = 1.d0/sqrt(2.d0)
      ovsqrt6 = ovsqrt3*ovsqrt2
      sqrt2 = 1.d0/ovsqrt2
      sqrt3 = 1.d0/ovsqrt3

c     print the material parameters in .dat file
      if (firstinc) then
        write(6,200) (PROPS(i),i=1,NPROPS)
        firstinc = .FALSE.
      end if

c ------------ THE CORE ROUTINE - RETURN-MAPPING ALGORITHM ------------
      call Return_Map(STRESS, DSTRAN, DTIME,
     +                STATEV, NSTATV,
     +                par_HT, np_HT, 
     +                par_EL, np_EL, 
     +                par_ES, np_ES, 
     +                par_RS, np_RS, 
     +                DDSDDE, infor)

      RETURN

C=======================================================================
C            _                     _           _     _             
C           | |                   | |         | |   | |            
C      ____ | | ____  ____ ____   | | _   ___ | | _ | | ____  ____ 
C     |  _ \| |/ _  |/ ___/ _  )  | || \ / _ \| |/ || |/ _  )/ ___)(_)
C     | | | | ( ( | ( (__( (/ /   | | | | |_| | ( (_| ( (/ /| |    
C     | ||_/|_|\_||_|\____\____)  |_| |_|\___/|_|\____|\____|_|    (_)
C     |_|                                                          
C     ------------------------------------------------------------------
C     format 200
C=======================================================================



      end subroutine umat

C=======================================================================
C            _                     _           _     _             
C           | |                   | |         | |   | |            
C      ____ | | ____  ____ ____   | | _   ___ | | _ | | ____  ____ 
C     |  _ \| |/ _  |/ ___/ _  )  | || \ / _ \| |/ || |/ _  )/ ___)(_)
C     | | | | ( ( | ( (__( (/ /   | | | | |_| | ( (_| ( (/ /| |    
C     | ||_/|_|\_||_|\____\____)  |_| |_|\___/|_|\____|\____|_|    (_)
C     |_|                                                          
C     ------------------------------------------------------------------
C     files inclusions
C=======================================================================
