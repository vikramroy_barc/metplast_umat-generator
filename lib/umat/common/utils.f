
C     This is to simplify the splitting of properties, 
C     see client/parser or generated umat

      subroutine Props_Assign(orig, orig_len,
     +                        dest, dest_len,
     +                        cut)
        implicit none 
        integer  orig_len, cut, dest_len
        real*8   orig(orig_len), dest(dest_len)

        integer  i

        if (dest_len .GE. 0) then
          do i=1,dest_len
            dest(i) = orig(i+cut)
          enddo
        endif

      end subroutine Props_Assign
      
