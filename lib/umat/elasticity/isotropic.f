C> @file isotropic.f 2-parameters linear elasticity

C> @author Baptiste Reyne: https://orcid.org/0000-0003-2310-864X

C> @brief linear elastic between rates of strain and stresses

C> @param[in] dStrain the strain increment or rate
C> @param[in] params <b>(2)</b> Elastic parameters:
C>    | symbol | name          | unit | default |
C>    |--------|-------------- |------|---------|
C>    | E      | Young modulus | MPa  | 70000.  |
C>    | nu     | Poisson ratio | -    | 0.33    |
C> @param[out] dStress the stress increment or rate

      SUBROUTINE dStrain_to_dStress(dStrain, params, dStress)
        implicit none
        !io
        real*8 dStrain(6), params(2), dStress(6) 
        !local
        real*8 E, nu, mu_2, k_3

c       Young modulus, Poisson's ratio
        E = params(1)
        nu   = params(2)
c       double of the shear modulus, triple of the bulk modulus
        mu_2 = E/(1.d0+nu)
        k_3  = E/(1.d0-2.d0*nu)

        dStress(1) = dStrain(1) * k_3
        dStress(2) = dStrain(2) * mu_2
        dStress(3) = dStrain(3) * mu_2
        dStress(4) = dStrain(4) * mu_2
        dStress(5) = dStrain(5) * mu_2
        dStress(6) = dStrain(6) * mu_2
      END SUBROUTINE dStrain_to_dStress

C> @param[out] dStress the stress increment or rate
C> @param[in] elparams Elastic parameters (see above)
C> @param[in] dStrain the strain increment or rate

      SUBROUTINE dStress_to_dStrain(dStress, elparams, dStrain)
        implicit none
        !io
        real*8 dStrain(6), elparams(2), dStress(6) 
        !local
        real*8 E, nu, mu_2, k_3

c       Young modulus, Poisson's ratio
        E    = elparams(1)
        nu   = elparams(2)
c       double of the shear modulus, triple of the bulk modulus
        mu_2 = E/(1.d0+nu)
        k_3  = E/(1.d0-2.d0*nu)

        dStrain(1) = dStress(1) / k_3
        dStrain(2) = dStress(2) / mu_2
        dStrain(3) = dStress(3) / mu_2
        dStrain(4) = dStress(4) / mu_2
        dStrain(5) = dStress(5) / mu_2
        dStrain(6) = dStress(6) / mu_2
      END SUBROUTINE dStress_to_dStrain      

C> @brief Fourth order Stiffness tensor in the natural basis.
C> @param[in] param Elastic parameters (see above)
C> @param[out] RES the Stiffness tensor

      SUBROUTINE Tangent_Stiffness(param, RES)
      implicit none
      !io
      real*8 param(2), RES(6,6)
      !local
      real*8 E, nu, mu_2, k_3
      integer i,j, ZERO

      E    = param(1)
      nu   = param(2)
      mu_2 = E/(1.d0+nu)
      k_3  = E/(1.d0-2.d0*nu)

      RES =reshape( (/
     +  k_3 , 0.d0, 0.d0, 0.d0, 0.d0, 0.d0,
     +  0.d0, mu_2, 0.d0, 0.d0, 0.d0, 0.d0,
     +  0.d0, 0.d0, mu_2, 0.d0, 0.d0, 0.d0,
     +  0.d0, 0.d0, 0.d0, mu_2, 0.d0, 0.d0,
     +  0.d0, 0.d0, 0.d0, 0.d0, mu_2, 0.d0,
     +  0.d0, 0.d0, 0.d0, 0.d0, 0.d0, mu_2 /),
     +  (/6,6/))

      END SUBROUTINE Tangent_Stiffness
      
C> @brief Fourth order compliance tensor in the natural basis.
C> @param[in] param Elastic parameters
C> @param[out] RES the Stiffness tensor

      SUBROUTINE Tangent_Compliance(param, RES)
      implicit none
      !io
      real*8 param(2), RES(6,6)
      !local
      real*8 E, nu, mu_2, k_3
      integer i,j

      E = param(1)
      nu   = param(2)
      mu_2 = (1.d0+nu)/E
      k_3  = (1.d0-2.d0*nu)/E
c     /!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\
c     Careful here the parameters are named identically as before but it
c     is the inverse this time.
c     /!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\
      RES =reshape( (/
     +  k_3 , 0.d0, 0.d0, 0.d0, 0.d0, 0.d0,
     +  0.d0, mu_2, 0.d0, 0.d0, 0.d0, 0.d0,
     +  0.d0, 0.d0, mu_2, 0.d0, 0.d0, 0.d0,
     +  0.d0, 0.d0, 0.d0, mu_2, 0.d0, 0.d0,
     +  0.d0, 0.d0, 0.d0, 0.d0, mu_2, 0.d0,
     +  0.d0, 0.d0, 0.d0, 0.d0, 0.d0, mu_2 /),
     +  (/6,6/))

      END SUBROUTINE Tangent_Compliance

C***********************************************************************
C=======================================================================
C-----------------------------------------------------------------------
C The 5 by 5 versions
c-----------------------------------------------------------------------

C> @param[in] dStrain the strain increment or rate
C> @param[in] elparams <b>(2)</b> Elastic parameters (see above)
C> @param[out] dStress the stress increment or rate

      SUBROUTINE dStrain_to_dStress_dev(dStrain, elparams, dStress)
        implicit none
        !io
        real*8 dStrain(5), elparams(2), dStress(5) 
        !local
        real*8 E, nu, mu_2, k_3

c       Young modulus, Poisson's ratio
        E = elparams(1)
        nu   = elparams(2)
c       double of the shear modulus, triple of the bulk modulus
        mu_2 = E/(1.d0+nu)
        k_3  = E/(1.d0-2.d0*nu)

        dStress(1) = dStrain(1) * mu_2
        dStress(2) = dStrain(2) * mu_2
        dStress(3) = dStrain(3) * mu_2
        dStress(4) = dStrain(4) * mu_2
        dStress(5) = dStrain(5) * mu_2
      END SUBROUTINE dStrain_to_dStress_dev

C> @param[out] dStress the stress increment or rate
C> @param[in] elparams <b>(2)</b> Elastic parameters (see above)
C> @param[in] dStrain the strain increment or rate

      SUBROUTINE dStress_to_dStrain_dev(dStress, elparams, dStrain)
        implicit none
        !io
        real*8 dStrain(5), elparams(2), dStress(5) 
        !local
        real*8 E, nu, mu_2, k_3

c       Young modulus, Poisson's ratio
        E    = elparams(1)
        nu   = elparams(2)
c       double of the shear modulus, triple of the bulk modulus
        mu_2 = E/(1.d0+nu)
        k_3  = E/(1.d0-2.d0*nu)

        dStrain(1)=dStress(1)/mu_2
        dStrain(2)=dStress(2)/mu_2
        dStrain(3)=dStress(3)/mu_2
        dStrain(4)=dStress(4)/mu_2
        dStrain(5)=dStress(5)/mu_2
      END SUBROUTINE dStress_to_dStrain_dev
