[[_TOC_]]


# Contributing

This project is under development at [NTNU/Physmet, Norway](https://www.ntnu.edu/physmet).
It is not yet open to external contributors, yet we are open to any comment and suggestion.

## Project structure

How the directory is organized:
- `./`
- `./examples/`: abaqus pre-formatted input files
- `./lib/`
  - `lib/client/`: python sources for the command line and graphical interfaces.
  - `lib/umat/`: set of fortran subroutines to pickup from when building a UMAT.
- `./README.md`
- `./CONTRIBUTING.md`: this file.


## Style guide

The codebase is a combination of Fortran subroutines and input files subjected to Abaqus requirements.
The clients and parser are written in python. 
The style requirements for each component are detailed below.

### Abaqus/inp

We follow Dassault System [2018 Input Syntax Rules](https://help.3ds.com/2018/english/dssimulia_established/SIMACAEMODRefMap/simamod-c-inputsyntax.htm?contextscope=all&id=4da0c908ad244458ab47a2cd7e5c5ff3).

### Python

- General **syntax** and naming conventions follow [PEP 8](https://peps.python.org/pep-0008/#naming-conventions).
- **Doc-strings** follow [PEP 257](https://peps.python.org/pep-0257/) and [numpy style guide](https://numpydoc.readthedocs.io/en/latest/format.html).
- **Testing** is done with [pytest](https://docs.pytest.org).

### Fortran

Fixed format (as opposed to [free format](https://www.mrao.cam.ac.uk/~pa/f90Notes/HTMLNotesnode44.html)) is required .

- Naming conventions.
  - `Sub_Routine_Names` are capitalized and underscore-separated.
  - `CONSTANTS` and `PARAMETERS` are full caps.
  - `variables` and `non_const` in lowercase.

- Naming scheme.
  - Extension: `.f`, or `.for` maybe for special uses.
  - `file_names.f` in small letters, underscore-separated 
  - indentations are made of 2 hard types spaces (no tab)

- **Docstrings**
  - Doxygen is used for fortran documentation and this project's special conventions (see below).

- Miscellaneous.
  - **/!\ TODO** format 200 is reserved for the log SUMMARY MSG. It shall be defined in ...?
  - Prefer allcaps for Fortran keywords. 
  - Preprocessing directives are forbidden: file inclusion is done with the FORTRAN procedure.

### Special conventions
The client will loop through the files chosen by the user and look for several special information.
- The amount of parameters
- Their description.
For `hardening_type` files, it will also scan for
- The amount of state variables
- Their description.

**Parameters detection**:
for every model file,  do so we use Doxygen docstrings and simply require that:
1. parameters are documented using Doxygen-keyword `@param[in] PARAMS`
2. and followed by their number under parenthesis (possible other characters between).
3. Parameters are detailed in a Markdown table left-aligned on column 7.
  The start is detected using character `|`, and the scan is interrupted when a new Doxygen keyword (`@keyword`) is encountered.
  If well formatted, a neat table is then obtained with the Doxygen pages.
4. Default values are provided in column 4.

The two last items are necessary for generating a sample input file.
A documentation could for example take the following form.
```fortran
C> @param[in] params <b>(3)</b> The parameters:
C
C>    |Symbol    | Name              |  Unit  | Default  |
C>    |----------|-------------------|--------| ---------|
C>    | sigma_0  | initial stress    |  MPa   | 100.     |
C>    | R_sat    | saturation stress |  MPa   | 300.     |
C>    | p_sat    | saturation strain |  -     |   0.5    |

````

**State variables detection**: 
- for files under `lib/umat/hardening_type/`, additional requirements apply.
  1. State variables are documented using Doxygen-keyword `@param[in,out] STATE_VARS`
  2. and followed by their number under parenthesis (possible other characters between).
  3. Symbol and descriptions are detailed  in a Markdown table left-aligned on column 7 (see previous item 3).
  4. The table contains symbols in column 1 and their description in column 2.

The two last items are necessary for generating a sample input file ().
A documentation could for example take the following form.
```fortran
C> @param[in] State_Vars (1) The isotropic hardening parameter
C
C>    | Symbol | Description          |  Unit  |
C>    |--------|----------------------|--------|
C>    | acp    | accum. plast. strain |  -     |

```

## Adding a model

The file requirements are detailed below.

| file                      | expected subroutine implementation               | comment |
| ------------------------- | ------------------------------------------------ | ------- |
| `hardening_type/*.f`      | `Return_Map()`                                   |         |
| `equivalent_stress/*.f`   | `Equivalent_Stress()`                            |         |
| `reference_stress/*.f`    | `Hardening_Rule()`, `Hardening_Rule_Deriv()`     |         |

## Sharing changes

A commit should contains a consistent set of modifications, clearly identified in the commit message.
The first line of the commit
- holds in less than 72 characters,
- neither starts with a capital nor ends with a period;
- completes the statement *"Once committed. these changes ... "* in the present tense.
  For example: *"refactor client exception types"*.

Example commit type (first word): add, fix, document, refactor, etc.
Example commit scope: client, umat, etc.

## Generating documentations

For the UMAT components, we use [Doxygen](https://doxygen.nl/index.html).
```bash
cd doc/
doxygen doxygen.cfg
xdg-open html/index.html
```
From the windows Doxygen wizard:
- file/open > `doc/doxygen.cfg`
- then next all and run.

For the python client, 
```bash
cd lib/
pydoc client # command line help tool
pydoc -b     # web documentation
```

## TODOS

**Current goals**

- [x] Clone to Physmet repo
- [x] Correct Tomas to Tomáš everywhere
- [x] Put natural notation routines into returnmap
- [ ] Implement kinematic

**Main goals**

- [ ] Implement distortional
- [ ] Implement change of material configuration

**Side quests**

- [ ] GUI contains a directory selector

- [ ] Fix the "long path issue" for fortran inclusion
- [ ] Provide Fortran/umat testers

- [ ] Cleanup and DOCUMENT, refactor all fortran files
- [ ] improve INP comment style (need same as continuum-plasticity)

- [ ] Geerator creates directory with project name

- [ ] Setup collaboration rules (propose one-file--one-maintainer---policy)

- [ ] Change Choleski functions to explicit names.
      Think of providing alternatives?


- [ ] Provide requirements, setup and pyinstaller
- [ ] Provide UMAT/VUMAT option with adapted sovers

- [ ] Possibly take out Line search from the return map to common

## Comments

- Preprocessing directives have a strong potential but was abandonned because Abaqus cannot be expected to accept it by default. It would be useful for headers, guard blocks and "global" parameters definitions.

- Flow rule is not an option but is rather included in the hardening type.
  This is not motivated by theory but implementation, since the return mapping algorithm heavily depends on the flow rule.

# References

```(pug)
                                                            .::-==========--------::.               
                                                        -+#@@@@@%######*#%@@@@@@@@@@@+              
                                                      =%@@@#+====++*****+--+%@@@@@@@@@*             
                                                   :=#@@%=:=#@@@@@@@@@@@@@%+-=#@@@@@@@@:            
                                                .+%@@@@@#+@@@@@@@@@@@@@@@@@@@#--%@@@@@@#            
                                               :@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*.+@@@@@@*           
                                              .:##*++=========+++**######%%@@@@@% :*@@@@@@-         
                                            .... .     .................:::-=*%@#    -+***:         
                                         ............................::::::::.:-:                   
                                       ..                ................:::::::::                  
                                     .                       ...   .........:::::::                 
                                          ..::..                .--...........::::::                
                                    :-=***+=---=++:         .-+**+-          ....::::.              
                                 .+*=-+@%.       ::      :=++=:.   :=++**#**++=-:..::::.            
                                  .     -++=:         .+*+-.    :#@@@@@@@@@@@@@@%*:...:::.          
                                 -=-.     .-#*       =#+.      =@@@@@#:.  .:+@@@@@#.....:::.        
                                *#:=+==.     ##     +:        .@@@@@#    -@@*+@@@@%......::::       
                        .       @:    =@=    .@-   :@         .@@@@@#    .==.-@@@@- .......:::      
                 .=#%%%-        *      *%     =@    #%.        +@@@@@+      -@@@@= ..........:.     
              .=#@@@@@@*               .%:     =#.   =#.        +@@@@@%*+*%@@@@@-    .........:.    
            -#@@@@@@@@@-                :#=     :%+    . ......:*@@@%%@@@@@@@@@#.     ........:.    
         .+%@@@@@@@@@@#                   .       ##    :=++++++=:.         ..:-+++-.   .......:    
       .*@@@@@@@*%@@@@=                            =#.                            .=++=.  .......   
       *@@@@@@+:=@@@@%.             :=*++=-:         .  :=*###########%%%%%%#*+-.    .=**-.......   
       %@@@@%:=%@@@@@+ .          =#*-.  .-=*=.       -%@@%#%@@@@@@@@@@@@@@@@@@@@%*-.   :=.......   
      :@@@@% #@@@@@@@: ..       =#+:         -#-     *@@+--*%@@@@@@@@@@@@@@@@@@@@@@@@%#=:    .....  
      =@@@@.=@@@@@@@%....     .%#.            .@:   #@#:=%@@@#-#@@*=@@@@@@@@@@@@@@@@@@@@@@#=:  ...  
      #@@@+.@@@@@@@@+.....   -@+    .:---:.    +*  =@@=#@@@@@#. ...+@@@@@@@@@@@@@@@@@@@@@@@@@#=...  
     -@@@@.+@@@@@@@@-......  -:  :*%@@@@@@@@#+.#- .@@@@@@#+@@@@@##@@@@@@@@@@@@@@@@@@@@@@@@@@@@@%-.  
    :@@@@@ *@@@@@@@%:.......    -@@@@@@@@@@@@@@*  =@@@@@@%.:@%==#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-  
   .%@@@@@.*@@@@@@@#.:........ -@@@@+:..:-=#@@%   +@@@@@@%: #@#- :%@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#  
   %@@@@@@=-@@@@@@@#.:........ %@@@:        *@:   -@@@@@%=+#@@@@%=.=@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  
  #@@@@@@@% %@@@@@@#:::...... :@@@* +@@*    #@    .@@@@@@@@@@@@@@@@=:+*=--------=*#@@@@@@@@@@@@@@=  
 =@@@@@@@@@+.@@@@@@%:::::.....-@@@@--#*:  :*@@.    %@@@@@@@@@@@@@@@@=  :+*####*+=-:.:+%@@@@@@@@#:   
 %@@@@@@@@@@+-#%%##*::::::.....@@@@@#+++#%@@@@:    #@@@@@@@@@@@@@@@%  #@@@@@@@@@@@@%+=#@@@@@@*:     
 #@@@@@@@@@@@*.     .:::::::...=%@@@@@@@@%#+=@+    %@@@@@@@@@@@@@@@= =@@@@@@@@@@@@@@@@@@@@@%:       
  +@@@@@@@@@@@@:      :::::::...:-++++=-.    -%   =@@@@@@@@@@@@@@@@- *@@@@@@@@@@@@@@@@@@@@%.        
   .+%@@@@@@@@@%        .:::::::...           %:  %@@@@@@@@@@@@@@@@= *@@@@@@@@@@@@@@@@@@@%.         
      :+%@@@@@@@           ..:::::........... *# .@@@@@@@@@@@@@@@@@* +@@@@@@@@@@@@@@@@@@#.          
         :+%@@%-               .:::.......... :@- @@@@@@@@@@@@@@@@@% :@@@@@@@@@@@@@@@@@+            
                                  .:::........ -+ #@@@@@@@@@@@@@@@@@: @@@@@@@@@@@@@@@%-             
                                    .::::........ +@@@@@@@@@@@@@@@@@*:@@@@@@@@@@@@@@*.              
                                      :::::........#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@%*:                
                                       .::::::......=%@@@@@@@@@@@@@@@@@@@@@@@#+-.                   
                                          ..:::::::...-*%@@@@@@@@@@@@@@@#+-.                        
                                              ..::::::..:-=*%@@@@@@%*=:.                            
                                                             .:::.                                  
```
